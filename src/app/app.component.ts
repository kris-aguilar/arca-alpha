import { Component, OnInit } from '@angular/core';
import { EventsService } from './core/services/events.service';
import { CustomEvents } from './core/constants/custom-events.constant';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'arca';
  confetti = false

  constructor(
    private eventsService: EventsService,
    private router: Router
   ) {
  }

  ngOnInit(): void {
    this.eventsService.on(CustomEvents.THROW_CONFETTI, (data) => {
      this.confetti = data
    })

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.confetti = false
      }
    })
  }
}
