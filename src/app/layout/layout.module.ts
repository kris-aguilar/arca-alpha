import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './footer/footer.component';
import { TopbarComponent } from './topbar/topbar.component';
import { LoaderComponent } from './loader/loader.component';
import { LoaderSectionComponent } from './loader-section/loader-section.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    FooterComponent,
    TopbarComponent,
    LoaderComponent,
    LoaderSectionComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    FooterComponent,
    TopbarComponent,
    LoaderComponent,
    LoaderSectionComponent
  ],
})
export class LayoutModule { }
