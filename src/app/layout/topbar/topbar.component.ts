import { Component, OnInit } from '@angular/core';
import { UserModel, Roles } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserDataService } from 'src/app/core/services/user-data.service';
import { distinctUntilChanged } from 'rxjs/operators';
import { EventsService } from 'src/app/core/services/events.service';
import { CustomEvents } from 'src/app/core/constants/custom-events.constant';
import { StorageKeys } from 'src/app/core/constants/storage-keys.constants';
import Swal from 'sweetalert2/dist/sweetalert2.min.js';
import { ToastrService } from 'ngx-toastr';
import { GenericStorageService } from 'src/app/core/services/generic-storage.service';
import { FireStorageService } from 'src/app/core/services/fire-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  user: UserModel
  photoURL = 'assets/images/anonymous.png'
  displayName = 'Anônimo'
  isAnon = true;
  roles: Roles = {
    admin: false,
    client: false,
    escolhido: false
  }

  constructor(
    private auth: AuthService,
    private userData: UserDataService,
    private eventsService: EventsService,
    private localStorage: GenericStorageService,
    private toastr: ToastrService,
    private fireStorage: FireStorageService,
    private router: Router
  ) { }

  async signOut() {
    try {
      this.loadingSwal('Saindo...');
      await this.auth.signOut();
      Swal.close();
      this.toastr.success('Você saiu da sua conta', 'Successo!', {
        positionClass: 'toast-top-left',
      });
      this.router.navigate([''])
    } catch (error) {
      Swal.close();
      console.error(error)
      this.toastr.error(error.code, 'Não foi possível atualizar sua foto de perfil')
    }
  }

  ngOnInit(): void {
    this.user = this.userData.get();
    this.addUser()

    this.auth.user$.pipe(
      distinctUntilChanged())
    .subscribe((res) => {
      if (res){
        this.roles = res.roles
        this.addUser()
      } else {
        this.addUser()
      }
    })

    this.auth.authState.pipe(
      distinctUntilChanged())
    .subscribe((res) => {
     if (res) this.isAnon = res.isAnonymous
    })

    this.eventsService.on(CustomEvents.UPDATE_NAME, (data) => {
      this.addUser()
    });
  }

  async updateName() {
    const pattern = /\s{1}/;
    const name = await Swal.fire({
      title: 'Insira seu nome e sobrenome',
      input: 'text',
      inputPlaceholder: 'Ex: João Oliveira',
      showCloseButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Você precisa escrever um nome!';
        } else if (!pattern.test(value)) {
          return 'Somente nome e sobrenome';
        }
      },
    });

    if (name.value) {
      try {
        this.loadingSwal('Adicionando nome...');
        const displayName = name.value;
        this.auth.updateProfile({ displayName });
        Swal.close();
        this.successSwal('Nome adicionado com sucesso!');
        this.eventsService.broadcast(CustomEvents.UPDATE_NAME, true);
      } catch (error) {
        Swal.close();
        console.error(error)
        this.toastr.error(error.code, 'Não foi possível atualizar sua foto de perfil')
      }
    } else {
      this.localStorage.save(StorageKeys.DONT_UPDATE_NAME, true);
    }
  }

  private successSwal(message: string) {
    return Swal.fire({
      title: message,
      icon: 'success',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true,
    });
  }

  private loadingSwal(title: string) {
    Swal.fire({
      title: title,
      text: 'Por favor aguarde',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }

  async updatePicture() {
    const file = await Swal.fire({
      title: 'Atualize sua Foto de Perfil',
      input: 'file',
      inputAttributes: {
        accept: 'image/*',
        'aria-label': 'Suba sua imagem aqui',
      },
    });

    if (file.value) {
      const reader = new FileReader();
      reader.readAsDataURL(file.value);
      reader.onload = async (e) => {
        try {
          const URL = await this.fireStorage.uploadUserFile(file.value);
          const profile = {
            photoURL: URL,
          };
          this.loadingSwal('Atualizando Foto...');
          await this.auth.updateProfile(profile);
          Swal.close()
          Swal.fire({
            title: 'Foto de perfil atualizada!',
            imageUrl: e.target.result,
            imageAlt: 'Foto nova',
          })
        } catch (error) {
          Swal.close()
          console.error(error)
          this.toastr.error(error.code, 'Não foi possível atualizar sua foto de perfil')
        }
      }
      reader.onerror = (error) => {
        Swal.close()
        console.error(error)
      }
    }
  }

  private addUser() {
    this.user = this.userData.get()
    if (this.user) {
      if (this.user.photoURL) this.photoURL = this.user.photoURL;
      if (this.user.displayName) {
        this.displayName = this.user.displayName
      } else {
        this.displayName = 'Anônimo'
      }
    } else {
      this.user = null
    }
  }
}
