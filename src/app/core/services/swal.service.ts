import { Injectable } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.min.js';

@Injectable({
  providedIn: 'root'
})
export class SwalService {

  constructor() { }

  successSwal(message: string) {
    return Swal.fire({
      title: message,
      icon: 'success',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true,
    });
  }

  loadingSwal(title?: string) {
    if (!title) title = 'Carregando...'
    Swal.fire({
      title: title,
      text: 'Por favor aguarde',
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }
}
