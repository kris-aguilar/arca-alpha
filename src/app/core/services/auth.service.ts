import { Injectable } from '@angular/core'
import { AngularFireAuth } from "@angular/fire/auth"
import { Observable, of } from 'rxjs'
import { UserModel } from '../models/user.model'
import { AngularFirestore, DocumentData } from '@angular/fire/firestore'
import { Router } from '@angular/router'
import { switchMap, take, map } from 'rxjs/operators'
import { AngularFirestoreDocument } from '@angular/fire/firestore'
import { UserDataService } from './user-data.service'
import { auth } from 'firebase/app'
import { environment } from 'src/environments/environment'
import { GenericStorageService } from './generic-storage.service'
import { StorageKeys } from '../constants/storage-keys.constants'
import { EventsService } from './events.service'
import { CustomEvents } from '../constants/custom-events.constant'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<UserModel>

  constructor(private afAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private router: Router,
    private userData: UserDataService,
    private localStorage: GenericStorageService,
    private eventsService :EventsService) {
    this.user$ = afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return firestore.doc<UserModel>(`users/${user.uid}`).valueChanges()
        } else {
          return of(null)
        }
      })
    )
  }

  get authState() {
    return this.afAuth.authState
  }

  async anonymousLogin() {
    const result = await this.afAuth.signInAnonymously()
    return this.updateUserData(result.user, null, null, true)
  }

  async linkAccount(provider: auth.AuthProvider) {
    const user = await this.afAuth.currentUser
    const credential = await user.linkWithPopup(provider)
    return this.updateUserData(credential.user)
  }

  async googleSignIn() {
    const provider = new auth.GoogleAuthProvider()
    const credential = await this.afAuth.signInWithPopup(provider)
    return this.updateUserData(credential.user)
  }

  async facebookSignIn() {
    const user = await this.linkFacebook()
    if (!user.emailVerified)
    {
      throw new Error("Você ainda não verificou seu email.");
    }
    return this.updateUserData(user)
  }

  async sendEmailLink(email: string) {
    const actionCodeSettings = {
      // Your redirect URL
      url: `${environment.url}/login-redirect`,
      handleCodeInApp: true,
    };

    try {
      const credential = await this.afAuth.sendSignInLinkToEmail(email, actionCodeSettings)
      this.localStorage.save(StorageKeys.EMAIL_SIGN_IN, email);
      return credential
    } catch (err) {
      return err.message;
    }
  }

  async confirmEmailLink(url: string) {
    try {
      if (this.afAuth.isSignInWithEmailLink(url)) {
        let email = this.localStorage.get(StorageKeys.EMAIL_SIGN_IN);
        // If missing email, prompt user for it
        if (!email) {
          throw Error("Email faltando!");
        } else {
          // Signin user and remove the email localStorage
          this.localStorage.remove(StorageKeys.EMAIL_SIGN_IN);
          const result = await this.afAuth.signInWithEmailLink(email, url);
          this.updateUserData(result.user)
          return result
        }

      }
    } catch (err) {
      return err;
    }
  }

  async getSignInMethods(email:string) {
    return await this.afAuth.fetchSignInMethodsForEmail(email)
  }

  async linkCredential(credential: auth.AuthCredential) {
    return (await this.afAuth.currentUser).linkWithCredential(credential)
  }

  async linkFacebook() {
    const provider = new auth.FacebookAuthProvider()
    const credential = await this.afAuth.signInWithPopup(provider)
    return credential.user
  }

  async sendVerificationEmail() {
    const user = await this.afAuth.currentUser
    return await user.sendEmailVerification()
  }

  async signOut() {
    this.userData.remove()
    await this.afAuth.signOut()
  }

  async getCurrentUser() {
    return await this.afAuth.currentUser
  }

  async updateProfile(profile: {
    displayName?: string,
    photoURL?: string
  }) {
    const user = await this.afAuth.currentUser
    if (profile.displayName && profile.displayName) {
      await user.updateProfile(profile)
    } else if (profile.displayName && !profile.photoURL) {
      const displayName = profile.displayName
      await user.updateProfile({displayName})
    } else if (!profile.displayName && profile.photoURL) {
      const photoURL = profile.photoURL
      await user.updateProfile({photoURL})
    }
    return this.updateUserData(user)
  }

  // Desestruturando o objeto para designar automaticamente os valores
  private updateUserData({ uid, email, displayName, photoURL, pedidoCount }: UserModel,
    firstName?: string, lastName?: string, isAnonymous?: boolean) {
    const userRef: AngularFirestoreDocument<UserModel> = this.firestore.doc(`users/${uid}`)
      if (displayName) {
        firstName = displayName.split(" ")[0]
        lastName = displayName.substring(displayName.lastIndexOf(" ") + 1)
      } else {
        if (isAnonymous) {
          displayName = 'Anônimo'
          pedidoCount = 0
        } else {
          firstName = null
        }
        lastName = null
      }

    if (!pedidoCount) {
      pedidoCount = 0
    }

    if (!photoURL) {
      photoURL = '/assets/images/anonymous.png'
    }

    const data: UserModel = {
      uid,
      email,
      displayName,
      firstName,
      lastName,
      photoURL,
      roles: {
        client: true
      },
      pedidoCount
    }
    this.userData.save({email, displayName, firstName, lastName, photoURL, pedidoCount})

    // Avisa o topbar pra ele se atualizar
    this.eventsService.broadcast(CustomEvents.UPDATE_NAME)

    // Merge: true só vai mudar os dados novos, assim evitando que ele limpe todo o usuário
    return userRef.set(data, { merge: true})
  }

  async updatePedidoCount(uid: string) {
    const ref = this.firestore.doc<UserModel>(`users/${uid}`)
    const user = await ref.get().toPromise()
    const data = user.data()

    if (data.pedidoCount >= 3) {
      throw Error('Número de pedidos excedido!')
    }

    return await ref.update({
      pedidoCount: data.pedidoCount + 1
    })
  }

  async getUsers() {
    const list = []
    const users = await this.firestore.collection('users').get().toPromise()
    users.forEach(user => {
      console.log(user.data())
    })

    return list
  }

  async clearAllPedidoCount() {
    const list = await this.getUsers()

    const batch = this.firestore.firestore.batch()

    for (let index = 0; index < list.length; index++) {
      const ref = this.firestore.collection('users').doc(list[index]).ref
      batch.update(ref, 'pedidoCount', 0)
    }
  }

  checkAuthorization(user: UserModel, allowedRoles: string[]): boolean {
    if (!user) return false
    for (const role of allowedRoles) {
      if (user.roles[role]) {
        return true
      }
    }
    return false
  }

  canRead(user: UserModel): boolean {
    const allowed = ['client', 'admin']
    return this.checkAuthorization(user, allowed)
  }

  canEdit(user: UserModel): boolean {
    const allowed = ['admin']
    return this.checkAuthorization(user, allowed)
  }

  canDelete(user: UserModel): boolean {
    const allowed = ['admin']
    return this.checkAuthorization(user, allowed)
  }
}
