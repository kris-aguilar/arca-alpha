import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class FireStorageService {

  constructor(
    private auth: AuthService,
    private afStorage: AngularFireStorage
  ) { }

  async uploadUserFile(file: File) {
    const user = await this.auth.getCurrentUser()
    const ref = this.afStorage.ref(`profile-pictures/${user.uid}/${file.name}`)

    await ref.put(file)

    return await ref.getDownloadURL().toPromise()
  }
}
