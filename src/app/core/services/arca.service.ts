import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { PedidosEAgradecimentos } from '../models/list.model';
import { firestore } from 'firebase/app'
import { UserModel } from '../models/user.model';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ArcaService {

  ref: AngularFirestoreDocument = this.afStore.collection('arca').doc(`arca`)

  constructor(
    private afStore: AngularFirestore,
    private afAuth: AuthService) {}

  async addPedidos(data: PedidosEAgradecimentos) {
    if (!data.photoURL) data.photoURL = '/assets/images/anonymous.png'

    const arrayUnion = firestore.FieldValue.arrayUnion(data)
    const update = await this.ref.update({
      pedidos: arrayUnion
    })
    await this.afAuth.updatePedidoCount(data.uid)
    return update
  }

  async getArcaData() {
    const data = await this.ref.get().toPromise()
    return data.data()
  }

  async updateEscolhido(uid: string, previous_uid: string) {
    await this.ref.update({
      escolhido: uid
    })
    const previousUserRef = this.afStore.doc<UserModel>(`users/${previous_uid}`)
    const previousUser = await previousUserRef.get().toPromise()
    const previousRoles = previousUser.data().roles
    previousRoles['escolhido'] = false

    const prevData: UserModel = {
      email: previousUser.data().email,
      roles:  previousRoles
    }
    await previousUserRef.set(prevData, {merge: true})

    console.log(previousRoles)
    const currentUserRef = this.afStore.doc<UserModel>(`users/${uid}`)
    const currentUser = await currentUserRef.get().toPromise()
    const currentRoles = currentUser.data().roles
    currentRoles['escolhido'] = true
    console.log(currentRoles)

    const currentData: UserModel = {
      email: currentUser.data().email,
      roles:  currentRoles
    }
    return await currentUserRef.set(currentData, {merge: true})
  }

  async clearPedidos() {
    const pedidos = {}
    return await this.ref.set({pedidos}, {merge: true})
  }

  async getEscolhido() {
    const arca = await this.ref.get().toPromise()
    const uid = arca.data().escolhido

    const userRef = this.afStore.doc<UserModel>(`users/${uid}`)
    const user = await userRef.get().toPromise()
    return user.data()
  }
}
