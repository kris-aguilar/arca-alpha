
export interface Arca {
  escolhido: string
  pedidos: Array<PedidosEAgradecimentos>
}

export interface PedidosEAgradecimentos {
  pedidos: Array<string>
  agradecimentos: Array<string>
  name?: string
  photoURL?: string
  uid?: string
}
