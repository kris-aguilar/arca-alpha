
export interface Roles {
  client?: boolean;
  escolhido?: boolean;
  admin?: boolean;
}

export interface UserModel {
  uid?: string;
  email: string;
  displayName?: string;
  firstName?: string;
  lastName?: string;
  photoURL?: string;
  roles?: Roles;
  password?: string;
  isAnonymous?: boolean;
  pedidoCount?: number;
}
