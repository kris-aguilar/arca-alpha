import { Component, OnInit, Output, EventEmitter, Input, HostListener, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.scss']
})
export class AddListComponent implements OnInit {

  @ViewChild('newItem') newItem: ElementRef
  @Output() addTodoChange = new EventEmitter();
  @Input() isPedidos: boolean;
  @Input() isLoggedIn: boolean;
  @Input() loading: boolean;


  constructor() { }

  ngOnInit(): void {
  }

  handleKeyboardPress(event: KeyboardEvent, value: string) {
    const isMobile =  /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    if (event.charCode === 13 && isMobile) {
      this.addTodo(value)
      this.newItem.nativeElement.value = ''
    }
  }
  addTodo(todo) {
    if (todo === '') {
      return
    }
    this.addTodoChange.next(todo);
  }

}
