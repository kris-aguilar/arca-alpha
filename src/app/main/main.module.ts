import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppMainComponent } from './app-main/app-main.component';
import { LoginComponent } from './login/login.component';
import { AddListComponent } from './add-list/add-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRedirectComponent } from './login-redirect/login-redirect.component';
import { ArcaComponent } from './arca/arca.component';
import { LayoutModule } from '../layout/layout.module';
import { PedidosComponent } from './pedidos/pedidos.component';
import { SorteioComponent } from './sorteio/sorteio.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminComponent } from './admin/admin.component';


@NgModule({
  declarations: [AppMainComponent, LoginComponent, AddListComponent, LoginRedirectComponent, ArcaComponent, PedidosComponent, SorteioComponent, AdminComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    NgbPaginationModule
  ],
  providers: []
})
export class MainModule { }
