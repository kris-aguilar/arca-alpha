import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.min.js';
import { ArcaService } from 'src/app/core/services/arca.service';
import { ToastrService } from 'ngx-toastr';
import { SwalService } from 'src/app/core/services/swal.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  escolhido: any
  loading: boolean

  constructor(
    private arcaService: ArcaService,
    private toastr: ToastrService,
    private swalService: SwalService
  ) { }

  ngOnInit(): void {
    this.getEscolhido()
  }

  async clearPedidos() {
    const confirm = await Swal.fire({
      title: 'Você tem certeza que quer limpar todos os pedidos?',
      icon: 'warning',
      text: 'Não tem volta!',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Voltar',
      confirmButtonText: 'Sim, limpar a Arca'
    })

    if (confirm.value) {
      try {
        this.swalService.loadingSwal('Limpando a Arca...')
        await this.arcaService.clearPedidos()
        Swal.close()
        this.swalService.successSwal('Arca limpada!')
      } catch (error) {
        Swal.close()
        console.log(error)
        this.toastr.error(error.code, 'Não foi possível limpar a Arca')
      }
    }
  }

  async getEscolhido() {
    try {
      this.loading = true
      this.escolhido = await this.arcaService.getEscolhido()
      this.loading = false
    } catch (error) {
      this.loading = false
      console.error(error)
      this.toastr.error(error.code, 'Escolhido não foi encontrado')
    }
  }

}
