import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PedidosEAgradecimentos, Arca } from 'src/app/core/models/list.model';
import { ArcaService } from 'src/app/core/services/arca.service';
import { EventsService } from 'src/app/core/services/events.service';
import { CustomEvents } from 'src/app/core/constants/custom-events.constant';
import Swal from 'sweetalert2/dist/sweetalert2.min.js';
import { DocumentData } from '@angular/fire/firestore/interfaces';

@Component({
  selector: 'app-sorteio',
  templateUrl: './sorteio.component.html',
  styleUrls: ['./sorteio.component.scss'],
})
export class SorteioComponent implements OnInit {
  @ViewChild('sorteando') sorteando: ElementRef;
  loading = false;
  escolhido: PedidosEAgradecimentos = null;
  answer: PedidosEAgradecimentos = {
    photoURL: '/assets/images/anonymous.png',
    pedidos: ['sdaddasds'],
    name: 'Anônimo',
    agradecimentos: ['sdaadadda'],
    uid: 'xRr5SSfsPvWZccXoGH1nuCNEyDt1',
  };

  constructor(
    private arcaService: ArcaService,
    private eventsService: EventsService
  ) {}

  ngOnInit(): void {}

  async sorteio() {
    this.eventsService.broadcast(CustomEvents.THROW_CONFETTI, false);
    this.escolhido = null;
    this.loading = true;
    try {
      const arcaData: DocumentData = await this.arcaService.getArcaData();
      const pedidos: Array<PedidosEAgradecimentos> = arcaData.pedidos;
      const previousEscolhido = arcaData.escolhido;

      if (pedidos.length <= 1) {
        this.loading = false;
        this.errorSwal(
          'Não existem pedidos o suficiente para realizar o sorteio!'
        );
        return;
      }

      let random: PedidosEAgradecimentos
      do {
        random = pedidos[Math.floor(Math.random() * pedidos.length)];
      } while (random.name === 'Anônimo' || random.uid === previousEscolhido);

      await this.arcaService.updateEscolhido(random.uid, previousEscolhido);
      this.escolhido = random;
      this.loading = false;
      this.eventsService.broadcast(CustomEvents.THROW_CONFETTI, true);
    } catch (error) {
      this.loading = false;
      console.error(error)
      this.errorSwal('Não foi possível fazer o sorteio')
    }
  }

  private errorSwal(message: string) {
    return Swal.fire({
      title: 'Opa!',
      text: message,
      icon: 'error',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true,
    });
  }
}
