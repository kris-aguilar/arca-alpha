import { Component, OnInit } from '@angular/core';
import { ArcaService } from 'src/app/core/services/arca.service';
import { PedidosEAgradecimentos } from 'src/app/core/models/list.model';
import { ToastrService } from 'ngx-toastr';
import html2pdf from 'html2pdf.js';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.scss'],
})
export class PedidosComponent implements OnInit {
  pedidos: Array<PedidosEAgradecimentos>;
  loading: boolean;
  printingPedidos = false;
  printingNomes = false;
  collectionSize = 0;
  pageSize = 10;
  page = 0;
  generatedPages = [];
  pdfLoading = false

  constructor(
    private arcaService: ArcaService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getPedidos();


    window.onafterprint = (event) => {
      this.finishPrint()
    }
  }

  public saveToPdf() {
    this.pdfLoading = true
    const opt = {
        margin: 0,
        // tslint:disable-next-line: max-line-length
        filename: `pedidos`,
        image: { type: 'jpeg', quality: 0.98 },
        html2canvas: { scale: 1 },
        jsPDF: { unit: 'cm', format: 'a4', orientation: 'portrait' }
    };
    let pages: any = ['pg1', 'pg2', 'pg3'];
    const lastPage: any = ['lastPage'];
    pages = pages.concat(this.generatedPages);
    pages = pages.concat(lastPage);
    for (let j = 0; j < pages.length; j++) {
        pages[j] = document.getElementById(pages[j]);
    }
    const id = document.getElementById('pedidos')
    let doc = html2pdf().set(opt).from(id).save();
    // doc.get('pedidos')
    // for (let j = 1; j < pages.length; j++) {
    //     doc = doc.get('pdf').then(
    //         pdf => { pdf.addPage(); }
    //     ).from(pages[j]).toContainer().toCanvas().toPdf();
    // }
    // doc.save();
    this.pdfLoading = false;
}

  async getPedidos() {
    this.loading = true;
    try {
      const arca = await this.arcaService.getArcaData();
      this.pedidos = arca.pedidos;
      this.collectionSize = this.pedidos.length;
      this.loading = false;
    } catch (error) {
      console.error(error);
      this.toastr.error(error.code, 'Não foi possível carregar pedidos');
    }
  }

  finishPrint() {
    this.printingPedidos = false;
    this.printingNomes = false;
    this.pageSize = 10;
  }

  printPedidos() {
    this.printingPedidos = true;
    this.pageSize = this.collectionSize;
    // this.saveToPdf()
    setTimeout(() => {
      window.print();
    }, 10);
  }

  printNomes() {
    this.printingNomes = true;
    this.pageSize = this.collectionSize;
    setTimeout(() => {
      window.print();
    }, 10);
  }
}
