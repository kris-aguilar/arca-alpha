import { Component, OnInit, Input } from '@angular/core';
import Swal from 'sweetalert2/dist/sweetalert2.min.js';
import { AuthService } from 'src/app/core/services/auth.service';
import { EventsService } from 'src/app/core/services/events.service';
import { CustomEvents } from 'src/app/core/constants/custom-events.constant';
import { FormGroup } from '@angular/forms';
import { UserModel } from 'src/app/core/models/user.model';
import { ToastrService } from 'ngx-toastr';
import { GenericStorageService } from 'src/app/core/services/generic-storage.service';
import { FireStorageService } from 'src/app/core/services/fire-storage.service';
import { auth } from 'firebase/app';
import { StorageKeys } from 'src/app/core/constants/storage-keys.constants';
import { SwalService } from 'src/app/core/services/swal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  @Input() user: UserModel = null;
  windowRef: any;
  phoneNumber;
  verificationCode: string;
  signInForm: FormGroup;
  displayName: string;

  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private eventsService: EventsService,
    private localStorage: GenericStorageService,
    private fireStorage: FireStorageService,
    private swalService: SwalService
  ) {}

  ngOnInit(): void {
    this.listenToEvents();
  }

  ngOnDestroy() {
    this.eventsService.removeListener(CustomEvents.ADD_NAME);
    this.eventsService.removeListener(CustomEvents.SIGN_OUT);
    this.eventsService.removeListener(CustomEvents.UPDATE_PICTURE);
  }

  private listenToEvents() {
    this.eventsService.on(CustomEvents.ADD_NAME, (data) => {
      if (data === 'UPDATE') this.inputNameSwal();
      if (data === 'ADD') this.addNameSwal();
    });
    this.eventsService.on(CustomEvents.SIGN_OUT, (data) => {
      this.signOut();
    });
    this.eventsService.on(CustomEvents.UPDATE_PICTURE, (data) => {
      this.inputPictureSwal();
    });
  }

  async signOut() {
    try {
      this.swalService.loadingSwal('Saindo...');
      await this.auth.signOut();
      Swal.close();
      this.toastr.success('Você saiu da sua conta', 'Successo!', {
        positionClass: 'toast-top-left',
      });
    } catch (error) {
      Swal.close();
      this.handleLoginErrors(error);
    }
  }

  private async linkUser() {
    const provider = new auth.FacebookAuthProvider();
    try {
      this.swalService.loadingSwal('Vinculando conta...');
      await this.auth.linkAccount(provider);
      Swal.close();
      this.swalService.successSwal('Conta vinculada com sucesso!');
    } catch (error) {
      Swal.close();
      this.handleLoginErrors(error);
    }
  }

  async loginGoogle() {
    try {
      this.swalService.loadingSwal('Logando usuário...');
      await this.auth.googleSignIn();
      Swal.close();
      this.toastr.success('Você está logado na sua conta', 'Successo!', {
        positionClass: 'toast-top-left',
      });
    } catch (error) {
      Swal.close();
      this.handleLoginErrors(error);
    }
  }

  async loginFacebook() {
    try {
      this.swalService.loadingSwal('Logando usuário...');
      await this.auth.facebookSignIn();
      Swal.close();
      this.toastr.success('Você está logado na sua conta', 'Successo!', {
        positionClass: 'toast-top-left',
      });
    } catch (error) {
      Swal.close();
      this.handleLoginErrors(error);
    }
  }

  private async accountAlreadyExists(
    email: string,
    methods: Array<string>,
    errorCredential: auth.AuthCredential
  ) {
    const linkSwal = await Swal.fire({
      title: 'Já existe uma conta vinculada a esse email!',
      text: `O endereço ${email} já está vinculado a um login
        ${this.getMethod(
          methods[0]
        )}. Deseja vincular essa conta com o login ${this.getMethod(
        errorCredential.signInMethod
      )}?`,
      icon: 'error',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não obrigado',
      confirmButtonText: 'Sim, vincular conta',
    });
    if (linkSwal.value) {
      this.handleLoginMethods(methods[0], email, errorCredential);
    }
  }

  private getMethod(method: string): string {
    switch (method) {
      case 'google.com':
        return 'Google';
      case 'facebook.com':
        return 'Facebook';
      case 'emailLink':
        return 'por link de email';
      default:
        return '';
    }
  }

  private async inputPictureSwal() {
    const file = await Swal.fire({
      title: 'Atualize sua Foto de Perfil',
      input: 'file',
      inputAttributes: {
        accept: 'image/*',
        'aria-label': 'Suba sua imagem aqui',
      },
    });

    if (file.value) {
      const reader = new FileReader();
      reader.readAsDataURL(file.value);
      reader.onload = async (e) => {
        try {
          const URL = await this.fireStorage.uploadUserFile(file.value);
          const profile = {
            photoURL: URL,
          };
          this.swalService.loadingSwal('Atualizando Foto...');
          await this.auth.updateProfile(profile);
          Swal.close();
          Swal.fire({
            title: 'Foto de perfil atualizada!',
            imageUrl: e.target.result,
            imageAlt: 'Foto nova',
          });
        } catch (error) {
          Swal.close();
          this.handleLoginErrors(error);
        }
      };
      reader.onerror = (error) => {
        this.handleLoginErrors(error);
      };
    }
  }

  private async addNameSwal() {
    const warning = await Swal.fire({
      title: 'Você ainda não tem um nome salvo!',
      text: 'Deseja adicionar um?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Não, obrigado',
      confirmButtonText: 'Sim, por favor',
    });

    if (warning.value) {
      this.inputNameSwal();
    }
  }

  private async inputNameSwal() {
    const pattern = /\s{1}/;
    const name = await Swal.fire({
      title: 'Insira seu nome e sobrenome',
      input: 'text',
      inputPlaceholder: 'Ex: João Oliveira',
      showCloseButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Você precisa escrever um nome!';
        } else if (!pattern.test(value)) {
          return 'Somente nome e sobrenome';
        }
      },
    });

    if (name.value) {
      try {
        this.swalService.loadingSwal('Adicionando nome...');
        const displayName = name.value;
        this.auth.updateProfile({ displayName });
        Swal.close();
        this.swalService.successSwal('Nome adicionado com sucesso!');
        this.eventsService.broadcast(CustomEvents.UPDATE_NAME, true);
      } catch (error) {
        Swal.close();
        this.handleLoginErrors(error);
      }
    } else {
      this.localStorage.save(StorageKeys.DONT_UPDATE_NAME, true);
    }
  }

  public async inputEmailSwal() {
    const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const email = await Swal.fire({
      title: 'Insira seu email',
      text: 'Enviaremos um link para você poder se logar',
      input: 'email',
      inputPlaceholder: 'Email',
      showCloseButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Você precisa escrever seu email!';
        } else if (!emailPattern.test(value)) {
          return 'Email inválido';
        }
      },
    });

    if (email.value) {
      try {
        this.swalService.loadingSwal('Enviando...');
        await this.auth.sendEmailLink(email.value);
        Swal.close();
        this.swalService.successSwal('Link de verificação enviado com sucesso!');
      } catch (error) {
        Swal.close();
        this.handleLoginErrors(error);
      }
    }
  }

  public async anonWarningSwal() {
    const warning = await Swal.fire({
      title: 'Atenção!',
      text: 'Usuários anônimos não podem ser sorteados!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Voltar',
      confirmButtonText: 'Prosseguir',
    });

    if (warning.value) {
      try {
        this.swalService.loadingSwal('Entrando como Anônimo...');
        await this.auth.anonymousLogin();
        Swal.close();
        this.toastr.success('Você está logado como Anônimo', 'Successo!', {
          positionClass: 'toast-top-left',
        });
      } catch (error) {
        Swal.close();
        this.handleLoginErrors(error);
      }
    }
  }

  private verificationSwal() {
    return Swal.fire({
      title: 'Opa!',
      text: 'Você ainda não verificou o seu email.',
      icon: 'error',
      confirmButtonText: 'Enviar verificação',
      showCloseButton: true,
    });
  }

  private async linkCredentialSuccess(credential: auth.AuthCredential) {
    return await Swal.fire({
      title: `Conta ${this.getMethod(
        credential.signInMethod
      )} vinculada com sucesso!`,
      icon: 'success',
      text: 'Você pode usá-la para se logar a partir de agora.',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true,
    });
  }

  private async handleLoginMethods(
    method: string,
    email: string,
    errorCredential: auth.AuthCredential
  ) {
    switch (method) {
      case 'google.com':
        try {
          this.swalService.loadingSwal('Vinculando conta...');
          await this.auth.googleSignIn();
          const user = await this.auth.getCurrentUser();
          await user.linkWithCredential(errorCredential);
          Swal.close();
          this.linkCredentialSuccess(errorCredential);
        } catch (error) {
          Swal.close();
          this.handleLoginErrors(error);
        }
        break;
      case 'facebook.com':
        try {
          this.swalService.loadingSwal('Vinculando conta...');
          await this.auth.facebookSignIn();
          const user = await this.auth.getCurrentUser();
          await user.linkWithCredential(errorCredential);
          Swal.close();
          this.linkCredentialSuccess(errorCredential);
        } catch (error) {
          Swal.close();
          this.handleLoginErrors(error);
        }
        break;
      case 'emailLink':
        const link = await Swal.fire({
          title:
            'Desculpa, mas não é possível vincular este tipo de conta no momento',
          icon: 'error',
          showCancelButton: true,
          timer: 5000,
          timerProgressBar: true,
        });

        if (link.value) {
          this.localStorage.save(StorageKeys.WANTS_TO_LINK_FACEBOOK, true);
          this.inputEmailSwal();
        } else {
          this.localStorage.save(StorageKeys.WANTS_TO_LINK_FACEBOOK, false);
        }
        break;
    }
  }

  private async handleLoginErrors(error) {
    console.error(error);
    if (error.message == 'Você ainda não verificou seu email.') {
      this.verificationSwal().then((res) => {
        if ('value' in res)
          this.auth.sendVerificationEmail().then(() => {
            this.swalService.successSwal('Email de confirmação enviado com sucesso!');
            this.auth.signOut();
          });
        if ('value' in res) this.auth.signOut();
      });
    } else {
      switch (error.code) {
        case 'invalid-argument':
          this.toastr.error(
            'Os dados inseridos estão incorretos',
            'Não foi possivel fazer o login.'
          );
          break;
        case 'auth/user-not-found':
          this.toastr.error(
            'Email não está registrado',
            'Não foi possivel fazer o login.'
          );
          break;
        case 'auth/cancelled-popup-request':
          this.toastr.error(
            'O popup foi fechado',
            'Não foi possivel fazer o login.'
          );
          break;
        case 'auth/popup-closed-by-user':
          this.toastr.error(
            'O popup foi fechado',
            'Não foi possivel fazer o login.'
          );
          break;
        case 'auth/account-exists-with-different-credential':
          const methods = await this.auth.getSignInMethods(error.email);
          this.accountAlreadyExists(error.email, methods, error.credential);
          break;
        default:
          this.toastr.error(error.code, 'Não foi possivel fazer o login.');
          break;
      }
    }
  }
}
