import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.min.js'

@Component({
  selector: 'app-login-redirect',
  templateUrl: './login-redirect.component.html',
  styleUrls: ['./login-redirect.component.scss']
})
export class LoginRedirectComponent implements OnInit {

  email: string;
  url: string

  constructor(
    private router: Router,
    private auth: AuthService
    ) { }

  ngOnInit(): void {
    this.url = this.router.url
    this.confirmLink()
  }

  private async confirmLink() {
    try {
      await this.auth.confirmEmailLink(this.url)
      Swal.close()
      await Swal.fire({
        title: 'Login efetuado!',
        text: 'Redirecionando à página principal...',
        icon: 'success',
        showCloseButton: true,
        timer: 5000,
        timerProgressBar: true
      })

      this.router.navigate([''])
    } catch (error) {
      console.error(error)
      if (error.message = 'Email faltando!') {
        this.emailMissingSwal()
      } else {
        await Swal.fire({
          title: 'Não foi possível se logar com sua conta',
          text: `motivo: ${error.code}`,
          timer: 6000,
          timerProgressBar: true
        })
      }


      this.router.navigate([''])
    }

  }

  private async emailMissingSwal() {
    const email = await Swal.fire({
      title: 'Email faltando!',
      text: 'Insira seu email novamente',
      input: 'email',
      inputPlaceholder: 'Email',
      showCloseButton: true,
      inputValidator: (value) => {
        if (!value) {
          return 'Você precisa escrever seu email!'
        }
      }
    })

    if (email.value) {
      this.loadingSwal('Validando...')
      this.confirmLink()
    }
  }

  private loadingSwal(title: string) {
    Swal.fire({
      title: title,
      text: 'Por favor aguarde',
      onBeforeOpen: () => {
        Swal.showLoading()
      },
    })
  }
}
