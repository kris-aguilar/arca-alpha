import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { PedidosEAgradecimentos } from 'src/app/core/models/list.model'
import { UserModel } from 'src/app/core/models/user.model'
import { AuthService } from 'src/app/core/services/auth.service'
import { UserDataService } from 'src/app/core/services/user-data.service'
import { distinctUntilChanged } from 'rxjs/operators'
import { EventsService } from 'src/app/core/services/events.service'
import { CustomEvents } from 'src/app/core/constants/custom-events.constant'

@Component({
  selector: 'app-app-main',
  templateUrl: './app-main.component.html',
  styleUrls: ['./app-main.component.scss']
})
export class AppMainComponent implements OnInit {

  windowRef: any
  phoneNumber
  verificationCode: string
  signInForm: FormGroup
  user: UserModel = null
  displayName: string = 'Anônimo'
  list: PedidosEAgradecimentos = {
    agradecimentos: [],
    pedidos: [],
    name: this.displayName
  };

  constructor(
    private auth: AuthService,
    private userData: UserDataService,
    private eventsService: EventsService
  ) { }

  ngOnInit(): void {
    this.user = this.userData.get();
    if (this.user) {
      this.setDisplayName()
    }
    this.auth.authState.pipe(
      distinctUntilChanged()
    )
    .subscribe(async (res) => {
      this.user = res;
      if (this.user) {
        this.setDisplayName()
        if (!this.user.isAnonymous && !this.user.displayName)
          this.eventsService.broadcast(CustomEvents.ADD_NAME, 'ADD');

      } else {
        this.displayName = 'Anônimo'
      }
    });

    this.eventsService.on(CustomEvents.UPDATE_NAME, (data) => {
      if (this.user) this.setDisplayName()
    })
  }

  private setDisplayName() {
    if (this.user.displayName) {
      this.displayName = this.user.displayName;
    } else {
      this.displayName = 'Anônimo'
    }
  }
}
