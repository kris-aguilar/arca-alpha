import { Component, OnInit, Input } from '@angular/core';
import { PedidosEAgradecimentos } from 'src/app/core/models/list.model';
import { UserModel } from 'src/app/core/models/user.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.min.js';
import { ArcaService } from 'src/app/core/services/arca.service';
import { ToastrService } from 'ngx-toastr';
import { GenericStorageService } from 'src/app/core/services/generic-storage.service';
import { StorageKeys } from 'src/app/core/constants/storage-keys.constants';

@Component({
  selector: 'app-arca',
  templateUrl: './arca.component.html',
  styleUrls: ['./arca.component.scss'],
})
export class ArcaComponent implements OnInit {
  @Input() user: UserModel = null;
  @Input() displayName: string;
  list: PedidosEAgradecimentos = {
    agradecimentos: [],
    pedidos: [],
  };
  arcaForm: FormGroup;
  loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private arcaService: ArcaService,
    private toastr: ToastrService,
    private localStorage: GenericStorageService
    ) {}

  ngOnInit(): void {
    this.arcaForm = this.formBuilder.group({
      agradecimentos: [this.list.agradecimentos],
      pedidos: [this.list.pedidos],
      name: [this.displayName],
    });
  }

  deletePedidos(index: number) {
    this.list.pedidos.splice(index, 1);
  }

  deleteAgradecimentos(index: number) {
    this.list.agradecimentos.splice(index, 1);
  }

  async submit() {
    this.loading = true;
    if (
      this.arcaForm.value.agradecimentos.length === 0 &&
      this.arcaForm.value.pedidos.length === 0
    ) {
      this.loading = false;
      await Swal.fire({
        title: 'Opa!',
        text: 'Você precisa escrever pelo menos um pedido ou agradecimento',
        icon: 'error',
        showCloseButton: true
      });
      return;
    }

    try {
      const data: PedidosEAgradecimentos = {
        agradecimentos: this.list.agradecimentos,
        pedidos: this.list.pedidos,
        name: this.user.displayName,
        photoURL: this.user.photoURL,
        uid: this.user.uid
      }

      await this.arcaService.addPedidos(data)
      this.loading = false;
      this.resetForm()
      this.successSwal('Pedido enviado com sucesso!')
    } catch (error) {
      this.loading = false;
      console.error(error)
      if (error.message === 'Número de pedidos excedido!') {
        Swal.fire({
          title: 'Opa!',
          text: 'Desculpa, mas você excedeu o número de pedidos enviados. Espere até sábado que vem para poder enviar mais!',
          icon: 'error',
          showCloseButton: true,
          timer: 5000,
          timerProgressBar: true,
        });
      } else {
        this.toastr.error(error.code, 'Ocorreu algum erro')
      }
    }
  }

  private resetForm() {
    this.list.pedidos = []
    this.list.agradecimentos = []
    const form = {
      pedidos: this.list.pedidos,
      agradecimentos: this.list.agradecimentos,
      name: this.displayName
    }
    this.arcaForm.reset(form)
  }

  private successSwal(message: string) {
    return Swal.fire({
      title: message,
      icon: 'success',
      showCloseButton: true,
      timer: 5000,
      timerProgressBar: true,
    });
  }

  addPedidos(item) {
    this.list.pedidos.push(item);
  }

  addAgradecimentos(item) {
    this.list.agradecimentos.push(item);
  }
}
