import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppMainComponent } from './main/app-main/app-main.component';
import { LoginRedirectComponent } from './main/login-redirect/login-redirect.component';
import { PedidosComponent } from './main/pedidos/pedidos.component';
import { AdminGuard } from './core/guards/admin.guard';
import { PedidosGuard } from './core/guards/pedidos.guard';
import { SorteioComponent } from './main/sorteio/sorteio.component';
import { AdminComponent } from './main/admin/admin.component';


const routes: Routes = [
  { path: '', component: AppMainComponent },
  { path: 'login-redirect', component: LoginRedirectComponent },
  { path: 'pedidos', canActivate: [PedidosGuard], component: PedidosComponent },
  { path: 'sorteio', canActivate: [AdminGuard], component: SorteioComponent },
  { path: 'admin', canActivate: [AdminGuard], component: AdminComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
