// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyArfiurSaKh3Qla9FvNWjSnpdWhr2hrMaE",
    authDomain: "arca-alpha.firebaseapp.com",
    databaseURL: "https://arca-alpha.firebaseio.com",
    projectId: "arca-alpha",
    storageBucket: "arca-alpha.appspot.com",
    messagingSenderId: "914640089688",
    appId: "1:914640089688:web:4d8f4dd6eee2e38b24102d"
  },
  url: 'https://localhost:4200'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
